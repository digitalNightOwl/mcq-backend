const router = require('express').Router();
const userController = require('../controllers/user.controller.js');
const commentController=require('../controllers/comment.controller.js');

const auth = require('../middleware/user.auth.js');
const upload = require('multer')();


router.post('/register', upload.any(),userController.register);
router.post('/verifyOtp', userController.verifyOtp);
router.post('/login', userController.login);
router.post('/forgotPassword', userController.forgetPassword);
router.post('/resetPassword', userController.resetPassword);
router.get('/getProfile', auth, userController.getProfile);
router.get('/getTestDetails/:id', auth, userController.getTestDetails);
router.get('/getTestQuestions/:id', auth, userController.myTestQuestions);
router.post('/updateProfile', auth, userController.updateProfile);
router.post('/verifyUptatedEmail',auth, userController.verifyEmail);
router.post('/resendOtp', userController.resendOtp);
router.post('/videoUrl', auth, userController.getQuestionPaper);
router.get('/getTests', auth, userController.getMyTests);
router.get('/getStudyMaterial/:id', auth, userController.getSutdyMaterials);
router.get('/getQuestionPaper/:id', auth, userController.TestDetails);
router.get('/start/:id', auth, userController.startTest);
router.post('/submitAnswer', auth, userController.submitAnswer);
router.get('/deleteTest/:id', userController.deleteTest);
router.post('/exitTest', auth,userController.exitTest);

router.post('/comment/:id',auth,commentController.createComment);
router.get('/comment/:id',auth,commentController.getCommentByUser);
router.patch('/comment/:id',auth,commentController.updateCommentByUser);




module.exports = router;