const router = require('express').Router();
const auth = require('../middleware/mediator.auth.js');
const mediatorController = require('../controllers/mediator.controller.js');

router.post('/login', mediatorController.login);
router.post('/qr/:id', mediatorController.generateQrCode);

module.exports = router;