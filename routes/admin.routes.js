const routes = require('express').Router();
const adminController = require('../controllers/admin.controller.js');
const commentController=require('../controllers/comment.controller.js');
const auth = require('../middleware/admin.auth.js');
const upload = require('multer')();

routes.post('/register', adminController.register);
routes.post('/login', adminController.login);
routes.get('/dashboard',auth, adminController.dashboard);
routes.get('/leaderBoard',auth, adminController.leaderBoard);
routes.get('/getUsers', auth, adminController.getUsers);
routes.get('/getQuestions',auth, adminController.getQuestions);
routes.get('/getQuestion/:id',auth, adminController.getQuestion);
routes.post('/addQuestion',auth, adminController.addQuestion);
routes.post('/uploadBulkQuestions', adminController.addBulkQuestions);
routes.patch('/updateQuestion/:id',auth, adminController.updateQuestion);
routes.delete('/deleteQuestion/:id',auth, adminController.deleteQuestion);
// routes.post('/generateQuestionPaper',auth, adminController.makeQuestionPaper);
// routes.get('/getQuestionPapers',auth, adminController.getQuestionPapers);
// routes.get('/getQuestionPaper/:id',auth, adminController.getQuestionPaper);
routes.post('/addCategory',upload.any() ,auth, adminController.addSubject);
routes.patch('/updateCategory/:id',upload.any() ,auth, adminController.uploadFileInSubject);
routes.get('/getCategories',auth, adminController.getSubjects);
routes.get('/getCategory/:id',auth, adminController.getSubject);
routes.delete('/deleteCategory',auth, adminController.deleteSubject);
routes.patch('/deleteCategoryUrl/:id',auth, adminController.deleteSubjectUrl);

routes.post('/addTopic',auth, adminController.addTopics);
routes.get('/getTopics',auth, adminController.getTopics);
routes.get('/getTopic/:id',auth, adminController.getTopic);
routes.delete('/deleteTopic',auth, adminController.deleteTopic);

routes.post('/addMediator',auth, adminController.addMediator);
routes.get('/tests',auth, adminController.getTests);
routes.get('/test/:_id',auth, adminController.getTest);
routes.get('/getMediators',auth, adminController.getMediators);
routes.delete('/deleteMediator/:id',auth, adminController.deleteMediator);

routes.post('/uploadTestInstruction',upload.any() , adminController.uploadTestInstruction);

routes.post('/qr/:id', auth, adminController.generateQrCode);
routes.get('/:id/testStatus', auth, adminController.getTestStatus);
routes.get('/leaderBoard', auth, adminController.leaderBoard);
routes.get('/averageReports', auth, adminController.averageReports);

routes.post('/comment/:id',auth,commentController.createComment);
routes.get('/comment/:id',auth,commentController.getCommentByUser);
routes.patch('/comment/:id',auth,commentController.updateCommentByUser);





module.exports = routes; 