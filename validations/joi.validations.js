const joi = require('joi');

const registerValidation = joi.object({
    name:joi.string().required(),
    email:joi.string().email().required(),
    password:joi.string().min(6).required(),
    employeeId:joi.string().required(),
})

const loginValidation = joi.object({
    employeeId:joi.string(),
    password:joi.string().min(6).required(),
})

const adminRegisterValidation = joi.object({
    name:joi.string().required(),
    email:joi.string().email().required(),
    password:joi.string().min(6).required(),
})

const addQuestionValidation = joi.object({
    img:joi.string(),
    question:joi.string().required(),
    option1:joi.string().required(),
    option2:joi.string().required(),
    option3:joi.string().required(),
    option4:joi.string().required(),
    answer:joi.string().required(),
    subject: joi.string().required(),
    topics: joi.string().required(),
    
})

const addMediatorValidation = joi.object({
    name:joi.string().required(),
    userId:joi.string().required(),
    password:joi.string().min(6).required(),
})

module.exports = {
    registerValidation,
    loginValidation,
    adminRegisterValidation,
    addQuestionValidation,
    addMediatorValidation,
}