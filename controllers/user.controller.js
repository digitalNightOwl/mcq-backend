const User = require("../models/user.model.js");
const Test = require("../models/test.model.js");
const questionsModel = require("../models/question.model.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {
  registerValidation,
  loginValidation,
} = require("../validations/joi.validations.js");
const sendOTP = require("../validations/sentOtp.js");
const qrcodeModel = require("../models/qrcode.model.js");
var CryptoJS = require("crypto-js");
// const categoryModel = require("../models/category.model.js");
const { uploadFile } = require("../utils/s3.js");
const subjectModel = require("../models/subject.model.js");
const topicModel = require("../models/topic.model .js");
const testModel = require("../models/test.model.js");
// const questionPaperModel = require("../models/questionPaper.model.js");

const routes = {};

// routes.register = async (req, res) => {
//   try {
//     const { name, email, password, employeeId } = req.body;

//     const { error } = registerValidation.validate(req.body);
//     if (error) return res.status(200).json({ error: error.details[0].message });

//     const emailExisting = await User.findOne({ email });

//     if (emailExisting && emailExisting.isVerified)
//       return res.status(200).json({ error: "Email already exists" });

//     if (emailExisting) {
//       await emailExisting.deleteOne();
//     }

//     const employeeIdExists = await User.findOne({ employeeId });
//     if (employeeIdExists)
//       return res.status(200).json({ error: "Employee Id already exists" });

//     const bcryptPassword = await bcrypt.hash(password, 12);
//     const user = await User.create({
//       name,
//       email,
//       password: bcryptPassword,
//       employeeId,
//       otp: Math.floor(1000 + Math.random() * 9000),
//       otpExpires: Date.now() + 10 * 60 * 1000,
//     });
//     // console.log(user, "user");

//     const response = await sendOTP(user.email, user.otp, "Email Verification OTP");
//     console.log(response, "response");
//     return res
//       .status(200)
//       .json({ result: user, message: "User registered successfully" });
//   } catch (error) {
//     console.log(error);
//     res.status(200).json({ error: "Something went wrong" });
//   }
// };




routes.register = async (req, res) => {
  try {
    const { name, email, password, employeeId } = req.body;
    console.log(req.body);
    const profilePicture=req.files[0];
     
    const { error } = registerValidation.validate(req.body);
    if (error) return res.status(200).json({ error: error.details[0].message });

    const emailExisting = await User.findOne({ email });

    if (emailExisting)
      return res.status(200).json({ error: "Email already exists" });

    // if (emailExisting) {
    //   await emailExisting.deleteOne();
    // }

    const employeeIdExists = await User.findOne({ employeeId });
    if (employeeIdExists)
      return res.status(200).json({ error: "Employee Id already exists" });

    const bcryptPassword = await bcrypt.hash(password, 12);

    const data=await uploadFile(
      profilePicture,
      `profilePicture/${name}-${profilePicture.originalname}`
    );

    console.log("data=",data);
    

    const user = await User.create({
      name,
      email,
      password: bcryptPassword,
      employeeId,
      profilePicture:data.Key,
      otpExpires: Date.now() + 10 * 60 * 1000,
    });
    return res
      .status(200)
      .json({ result: user, message: "User registered successfully" });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};



routes.verifyOtp = async (req, res) => {
  try {
    const { id, otp } = req.body;

    if (!id || !otp)
      return res
        .status(200)
        .json({ error: "Please enter all the fields", result: false });

    const user = await User.findById(id);

    if (user.otpExpires < Date.now())
      return res.status(200).json({ error: "OTP expired", result: false });

    if (user.otp !== otp)
      return res.status(200).json({ error: "Invalid OTP", result: false });

    user.isVerified = true;

    await user.save();

    return res
      .status(200)
      .json({ message: "OTP verified successfully", result: true });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

// routes.login = async (req, res) => {
//   try {
//     const { employeeId, password } = req.body;

//     const { error } = loginValidation.validate(req.body);

//     if (error) return res.status(200).json({ error: error.details[0].message });

//     if (!employeeId || !password)
//       return res.status(200).json({ error: "Please enter all the fields" });

//     const user = await User.findOne({ employeeId });

//     if (!user) return res.status(200).json({ error: "User not found" });

//     if (!user.isVerified)
//       return res.status(200).json({ error: "Please verify your email" });

//     const isMatch = await bcrypt.compare(password, user.password);

//     if (!isMatch) return res.status(200).json({ error: "Invalid credentials" });

//     const token = jwt.sign(
//       { id: user._id, role: "user" },
//       process.env.JWT_SECRET,
//       { expiresIn: "1h" }
//     );

//     return res.status(200).json({ result: user, token });
//   } catch (error) {
//     console.log(error);
//     res.status(200).json({ error: "Something went wrong" });
//   }
// };



routes.login = async (req, res) => {
  try {
    const { employeeId, password,fcmToken } = req.body;
    const { error } = loginValidation.validate(req.body);

    

    if (error) return res.status(200).json({ error: error.details[0].message });

    if (!employeeId || !password)
      return res.status(200).json({ error: "Please enter all the fields" });

    const user = await User.findOneAndUpdate({ employeeId },{fcmToken},{new:true});

    if (!user) return res.status(200).json({ error: "User not found" });

    // if (!user.isVerified)
    //   return res.status(200).json({ error: "Please verify your email" });

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) return res.status(200).json({ error: "Invalid credentials" });

    const token = jwt.sign(
      { id: user._id, role: "user" },
      process.env.JWT_SECRET,
      { expiresIn: "1h" }
    );

    return res.status(200).json({ result: user, token });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};




// routes.forgetPassword = async (req, res) => {
//   try {
//     const { email } = req.body;

//     if (!email)
//       return res.status(200).json({ error: "Please enter all the fields" });

//     const newOtp = Math.floor(1000 + Math.random() * 9000);

//     const user = await User.findOne({ email });
//     if (!user) return res.status(200).json({ error: "User not found" });

//     user.otp = newOtp;
//     user.otpExpires = Date.now() + 10 * 60 * 1000;

//     await user.save();

//     sendOTP(user.email, newOtp, "Forgot Password OTP");

//     return res
//       .status(200)
//       .json({ message: "OTP sent successfully", id: user._id });
//   } catch (error) {
//     console.log(error);
//     res.status(200).json({ error: "Something went wrong" });
//   }
// };


routes.forgetPassword = async (req, res) => {
  try {
    const { email } = req.body;

    if (!email)
      return res.status(200).json({ error: "Please enter all the fields" });

    // const newOtp = Math.floor(1000 + Math.random() * 9000);

    const user = await User.findOne({ email });
    if (!user) return res.status(200).json({ error: "User not found" });

    // user.otp = newOtp;
    // user.otpExpires = Date.now() + 10 * 60 * 1000;

    await user.save();

    // sendOTP(user.email, newOtp, "Forgot Password OTP");

    // return res
    //   .status(200)
    //   .json({ message: "OTP sent successfully", id: user._id });
    return res.status(200).json({message: "User found",id: user._id});
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};





// routes.resetPassword = async (req, res) => {
//   try {
//     const { id, password } = req.body;

//     if (!id || !password)
//       return res.status(200).json({ error: "Please enter all the fields" });

//     if (password.length < 6)
//       return res
//         .status(200)
//         .json({ error: "Password must be atleast 6 characters long" });

//     const user = await User.findById(id);

//     if (!user) return res.status(200).json({ error: "User not found" });

//     const isMatch = await bcrypt.compare(password, user.password);

//     if (isMatch)
//       return res
//         .status(200)
//         .json({ error: "New password cannot be same as old password" });

//     const bcryptPassword = await bcrypt.hash(password, 12);
//     user.password = bcryptPassword;
//     await user.save();

//     return res.status(200).json({ message: "Password updated successfully" });
//   } catch (error) {
//     console.log(error);
//     res.status(200).json({ error: "Something went wrong" });
//   }
// };



routes.resetPassword = async (req, res) => {
  try {
    const { id, password } = req.body;

    if (!id || !password)
      return res.status(200).json({ error: "Please enter all the fields" });

    if (password.length < 6)
      return res
        .status(200)
        .json({ error: "Password must be atleast 6 characters long" });

    const user = await User.findById(id);

    if (!user) return res.status(200).json({ error: "User not found" });

    const isMatch = await bcrypt.compare(password, user.password);

    if (isMatch)
      return res
        .status(200)
        .json({ error: "New password cannot be same as old password" });

    const bcryptPassword = await bcrypt.hash(password, 12);
    user.password = bcryptPassword;
    await user.save();

    return res.status(200).json({ message: "Password updated successfully" });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};


routes.getProfile = async (req, res) => {
  try {
    const user = await User.findById(req.userId).select(
      "-password -otp -isVerified -tests"
    );
    return res.status(200).json({ result: user });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.getTests = async (req, res) => {
  try {
    const tests = await Test.find({ user: req.userId }).populate("subject").select("subject");
    if (!tests) return res.status(200).json({ error: "Test not found" });

    return res.status(200).json({ result: tests });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.getTestDetails = async (req, res) => {
  try {
    const test = await Test.findById(req.params.id);

    if (!test) return res.status(200).json({ error: "Test not found" });
    const skipped = test.answers.reduce((n, x) => n + (x === "skipped"), 0);
    return res.status(200).json({
      testId: test._id,
      score: test.score,
      totalQuestions: test.questions.length,
      skipped,
      attempted: test.questions.length - skipped,
      timeTaken: 3600 - test.currentTimer,
      isCompleted: test.isCompleted,
      started: test.isStarted,
    });

  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.myTestQuestions = async (req, res) => {
  try {
    const test = await Test.findById(req.params.id);

    if (!test) return res.status(200).json({ error: "Test not found" });
    if(!test.isStarted) return res.status(200).json({ error: "Test not started" });
    const questions = test.questions.map((question, index) => {
      return {
        question: question.question,
        Option1: question.option1,
        Option2: question.option2,
        Option3: question.option3,
        Option4: question.option4,
        answer: question.answer,
        myAnswer: test.answers[index],
      };
    });

    return res.status(200).json({ result: questions });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

// routes.myTestQuestions = async (req, res) => {
//   try {

//     const test = await Test.findOne({ user: req.userId })
// // console.log(test,"test");
//     if (!test) return res.status(200).json({ error: "Test not found" });

//     const questionsPromises = test.questions.map(async (id) => {
//         const question = await questionModel.findById(id);
//         if (question !== null) {
//           console.log(question,"question")
//             return {
//                 question: question.question,
//                 Option1: question.option1,
//                 Option2: question.option2,
//                 Option3: question.option3,
//                 Option4: question.option4,
//             };
//         }
//         return;
//     });
//     console.log(test)
//     // Wait for all promises to resolve
//     const questions = await Promise.all(questionsPromises);

//     const newQuestion = questions.map((question,index) => {
//       console.log(question,"question");
//       if (question !== undefined) {
//         return {
//           question: question.question,
//                 Option1: question.Option1,
//                 Option2: question.Option2,
//                 Option3: question.Option3,
//                 Option4: question.Option4,
//                 myAnswer: test.answers[index],
//         };
//       }
//     });

//     console.log(newQuestion,"newQuestion" );

//     console.log(questions,"questions");

//     //filter out undefined values
//     const filteredQuestions = newQuestion.filter((question) => question !== undefined);

//     return res.status(200).json({ result: filteredQuestions });
//   } catch (error) {
//     console.log(error);
//     res.status(200).json({ error: "Something went wrong" });
//   }
// };

routes.deleteTest = async (req, res) => {
  try {
    const id = req.params.id;
    const test = await Test.findOne({ user: id });

    if (!test) return res.status(200).json({ error: "Test not found" });

    await test.deleteOne();

    const user = await User.findById(id);
    user.test = null;
    await user.save();

    return res.status(200).json({ message: "Test deleted successfully" });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.updateProfile = async (req, res) => {
  try {
    const { name, email, password, newPassword } = req.body;

    if (!name && !email && !(password && newPassword))
      return res.status(200).json({ error: "Please enter all the fields" });

    const user = await User.findById(req.userId);

    if (!user) return res.status(200).json({ error: "User not found" });

    if (name) user.name = name;
    if (password && newPassword) {
      if (password.length < 6)
        return res
          .status(200)
          .json({ error: "Password must be atleast 6 characters long" });

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch)
        return res.status(200).json({ error: "Invalid credentials" });
      const bcryptPassword = await bcrypt.hash(newPassword, 12);
      user.password = bcryptPassword;
    }

    if (email) {
      if (email === user.email) {
        console.log("same")
        return res.status(200).json({ error: "Email is same" });
      }

      const ifEmail = await User.findOne({ email });
      if (ifEmail)
        return res.status(200).json({ error: "Email already exists" });

      user.otp = Math.floor(1000 + Math.random() * 9000);
      // sendOTP(email, user.otp, "Email Verification OTP");
      user.otpExpires = Date.now() + 10 * 60 * 1000;
      user.email = email;
      await user.save();
      // return res
      //   .status(200)
      //   .json({ message: "OTP sent successfully", email: email });
    }

    // await user.save();
    console.log({ message: "Profile updated successfully" });

    return res.status(200).json({ message: "Profile updated successfully" });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.verifyEmail = async (req, res) => {
  try {
    const { otp, email } = req.body;

    if (!otp || !email)
      return res
        .status(200)
        .json({ error: "Please enter all the fields", result: false });

    const user = await User.findById(req.userId);

    if (user.otpExpires < Date.now())
      return res.status(200).json({ error: "OTP expired", result: false });

    if (user.otp !== otp)
      return res.status(200).json({ error: "Invalid OTP", result: false });

    // user.isVerified = true;
    user.email = email;
    await user.save();

    return res
      .status(200)
      .json({ message: "OTP verified successfully", result: true });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.resendOtp = async (req, res) => {
  try {
    const { id } = req.body;

    if (!id)
      return res
        .status(200)
        .json({ error: "Please enter all the fields", result: false });

    const user = await User.findById(id);

    // if(user.otpExpires < Date.now()) return res.status(200).json({ error: "OTP expired", result: false });

    user.otp = Math.floor(1000 + Math.random() * 9000);
    user.otpExpires = Date.now() + 10 * 60 * 1000;
    await user.save();

    sendOTP(user.email, user.otp, "Email Verification OTP");

    return res
      .status(200)
      .json({ message: "OTP sent successfully", result: true });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.getQuestionPaper = async (req, res) => {
  try {
    const { id } = req.body;

    var bytes = CryptoJS.AES.decrypt(id, "secret key 123");
    var originalText = bytes.toString(CryptoJS.enc.Utf8);

    const isexists = await qrcodeModel.findOne({ qrcode: originalText });

    if (!isexists) return res.status(200).json({ error: "Invalid QR Code" });

    if (isexists.expiresAt < Date.now())
      return res.status(200).json({ error: "QR Code expired" });

    const user = await User.findById(req.userId);

    const subject = await subjectModel.findById(isexists.subject);
    
    if(!subject) return res.status(200).json({ error: "subject not found || try again with another QR code" });
    const topic = await topicModel.findById(isexists.topics);
    if(!topic) return res.status(200).json({ error: "topic not found || try again with another QR code" });
    // console.log(subject, "subject");

    // check if user already has a test of this subject

    const testexist = await Test.findOne({ user: req.userId, subject: subject._id,topic:topic._id });

    if (testexist)
      return res.status(200).json({ error: "Test already exists" });

    const questions =  await questionsModel.find({ subject: subject.name,topics: topic.name });
    if(!questions) return res.status(200).json({ error: "Questions not found" }); 
    // console.log(questions, "questions");
    const shuffled = questions.sort(() => 0.5 - Math.random());
    // console.log(shuffled, "shuffled");
    const totalQuestions = shuffled.length > 20 ? 20 : shuffled.length;

    let selected = shuffled.slice(0, totalQuestions);
    // console.log(selected, "selected");
    const test = await Test.create({
      questions: selected,
      user: req.userId,
      currentTimer: 3600,
      subject: subject._id,
      topic:topic._id
    });

    // console.log(test, "test");

    user.test.push(test._id);

    await user.save();
    if (!test)
      return res
        .status(200)
        .json({ error: "Something went wrong. try again after sometime" });

    return res.status(200).json({
      // numberOfQuestions: test.questions?.length,
      // time: test.currentTimer,
      urls: subject.urls,
      testId: test._id,
    });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.getMyTests = async (req, res) => {
  try {

    const tests = await Test.find({ user: req.userId },{subject:1,topic:1}).populate({path:"subject",select:"name"}).populate({path:"topic",select:"name"});
    if(!tests) return res.status(200).json({ error: "Test not found" });

    return res.status(200).json({ result: tests });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
}

// routes.getSutdyMaterials = async (req, res) => {
//   try {
//     const categories = await subjectModel.find();
//     if(!categories) return res.status(200).json({ error: "subject not found" });

//     return res.status(200).json({ result: categories });
//   } catch (error) {
//     console.log(error);
//     res.status(200).json({ error: "Something went wrong" });
//   }
// };

routes.getSutdyMaterials = async (req, res) => {
  try {
    const subject = await subjectModel.findById(req.params.id);
    if(!subject) return res.status(200).json({ error: "Subject not found" });

    return res.status(200).json({ result: subject.urls });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.TestDetails = async (req, res) => {
  try {
    const test = await Test.findById(req.params.id);
    if (!test) return res.status(200).json({ error: "Test not found" });

    return res.status(200).json({
      numberOfQuestions: test.questions?.length,
      time: test.currentTimer,
      testId: test._id,
    });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

routes.startTest = async (req, res) => {
  try {
    const { id } = req.params;

    const test = await Test.findById(id)

    if (!test)
      return res.status(200).json({ error: "Question paper not found" });
    
    if (test.isStarted){
      return res.status(200).json({ error: "Test was already started" });
    }

    const currentQuestion = test.questions[test.currentQuestion];

    // test.currentQuestion += 1;
    test.isStarted = true;

    await test.save();

    return res.status(200).json({
      testId: test._id,
      question: currentQuestion,
      currentQuestion: 1,
      numberOfQuestions: test.questions?.length,
    });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};

// routes.getQuestions = async (req, res) => {
//     try {
//         const { id } = req.params;
//         const test = await Test.findById(id).populate("questionPaper");
//         if(!test) return res.status(200).json({ error: "Test not found" });
//         const questionPaper = test.questionPaper;
//         const questions = await Question.find({ _id: { $in: questionPaper.questions } });
//         return res.status(200).json({ result: questions, time: questionPaper.time, difficulty: questionPaper.difficulty });
//     } catch (error) {
//         console.log(error);
//         res.status(200).json({ error: "Something went wrong" });
//     }
// }

routes.submitAnswer = async (req, res) => {
  try {
    const { testId, answer, timer } = req.body;

    if (!testId || !answer)
      return res.status(200).json({ error: "Please enter all the fields" });

    const test = await Test.findById(testId).populate({
      path: "questions",
    });

    if (!test) return res.status(200).json({ error: "Test not found" });

    const question = test.questions[test.currentQuestion];

    if (!question)
      return res
        .status(200)
        .json({ error: "Question not found OR test completed" });
    console.log(question);

    if (question.answer == answer) {
      console.log("correct");
      test.score += 1;
    }

    test.answers.push(answer);
    test.currentQuestion += 1;
    test.currentTimer = timer;
    await test.save();

    if (test.currentQuestion === test.questions.length) {
      test.isCompleted = true;
      await test.save();
      const skipped = test.answers.reduce((n, x) => n + (x === "skipped"), 0);
      return res.status(200).json({
        message: "Test completed successfully",
        totalQuestions: test.questions.length,
        skipped,
        attempted: test.questions.length - skipped,
        timeTaken: 3600 - test.currentTimer,
        question: null,
      });
    }
    const nextQuestion = test.questions[test.currentQuestion];

    return res.status(200).json({
      question: nextQuestion,
      currentQuestion: test.currentQuestion + 1,
      numberOfQuestions: test.questions?.length,
    });
  } catch (error) {
    console.log(error);
    res.status(200).json({ error: "Something went wrong" });
  }
};


routes.exitTest=async (req,res)=>{
    const { testId, answer, timer}=req.body;

     const test=await testModel.findById(testId).populate({
      path: "questions",
    });

     if(!test) return  res.status(404).json({ error: "Test Not Found" });
     
     const question = test.questions[test.currentQuestion];
     
     if(!question) return  res.status(404).json({ error: "Question Not Found OR Test Compeleted" });

     if (question.answer == answer) {
      console.log("correct");
      test.score += 1;
    }
    test.answers.push(answer);
    test.currentTimer = timer;
    await test.save();    

    const totalQuestion=test.questions;
    const totalAnswer=test.answers;
    let skipped = test.answers.reduce((n, x) => n + (x === "skipped"), 0);

    console.log(totalQuestion.length,totalAnswer.length)  
    while(totalQuestion.length> totalAnswer.length){ 
              test.answers.push("skipped");
             skipped++;  
             console.log("start") ;
    }
    await test.save();

  return res.status(200).json({
    message: "Test completed successfully",
    totalQuestions: test.questions.length,
    skipped,
    attempted: test.questions.length - skipped,
    timeTaken: 3600 - test.currentTimer,
    question: null,
  });
}
module.exports = routes;
