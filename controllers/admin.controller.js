const admin = require("../models/admin.model.js");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const {
  adminRegisterValidation,
  addQuestionValidation,
  addMediatorValidation,
} = require("../validations/joi.validations.js");
const userModel = require("../models/user.model.js");
const questionModel = require("../models/question.model.js");
// const questionPaperModel = require("../models/questionPaper.model.js");
const testModel = require("../models/test.model.js");
const categoryModel = require("../models/category.model.js");
const mediatorModel = require("../models/mediator.model.js");
const {
  uploadFile,
  getSignedUrl,
  getFiles,
  deleteFile,
} = require("../utils/s3.js");
const qr = require("qr-image");
var CryptoJS = require("crypto-js");
const qrcodeModel = require("../models/qrcode.model");
const subjectModel = require("../models/subject.model.js");
const topicModel = require("../models/topic.model .js");
const { Mongoose } = require("mongoose");
const commentModel = require("../models/comment.model.js");
const routes = {};

routes.register = async (req, res) => {
  try {
    const { name, email, password, fcmToken } = req.body;

    const { error } = adminRegisterValidation.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message });

    const ifAdminExists = await admin.findOne({ email });

    if (ifAdminExists)
      return res.status(400).json({ error: "Email already exists" });

    const bcryptPassword = await bcrypt.hash(password, 12);
    const user = await admin.create({
      name,
      email,
      password: bcryptPassword,
      fcmToken,
    });

    return res
      .status(201)
      .json({ result: user, message: "Admin registered successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.login = async (req, res) => {
  try {
    const { email, password, fcmToken } = req.body;

    if (!email || !password)
      return res.status(400).json({ error: "Please enter all the fields" });

    const user = await admin.findOneAndUpdate(
      { email },
      { fcmToken },
      { new: true }
    );

    if (!user) return res.status(404).json({ error: "User not found" });

    const isPasswordCorrect = await bcrypt.compare(password, user.password);

    if (!isPasswordCorrect)
      return res.status(400).json({ error: "Invalid credentials" });

    const token = jwt.sign(
      { email: user.email, id: user._id, role: "admin" },
      process.env.JWT_SECRET,
      { expiresIn: "1d" }
    );
    console.log(token);

    return res.status(200).json({ result: user, token });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.dashboard = async (req, res) => {
  try {
    const users = await userModel.find().countDocuments();
    const questions = await questionModel.find().countDocuments();
    // const questionPapers = await questionPaperModel.find().countDocuments();
    const tests = await testModel.find().countDocuments();
    const completedTests = await testModel
      .find({ isCompleted: true })
      .countDocuments();
    const incompleteTests = await testModel
      .find({ isCompleted: false })
      .countDocuments();
    const subject = await subjectModel.find().countDocuments();
    //questions in each category
    const subjectWithTopics = await subjectModel.aggregate([
      {
        $lookup: {
          from: "topics",
          localField: "name",
          foreignField: "subject",
          as: "topics",
        },
      },
      {
        $project: {
          name: 1,
          topics: { $size: "$topics" },
        },
      },
    ]);

    const subjectWithQuestions = await subjectModel.aggregate([
      {
        $lookup: {
          from: "questions",
          localField: "name",
          foreignField: "subject",
          as: "question",
        },
      },
      {
        $project: {
          name: 1,
          questions: { $size: "$question" },
        },
      },
    ]);

    const subjectWithTests = await subjectModel.aggregate([
      {
        $lookup: {
          from: "tests", // Collection name for Test model
          localField: "_id",
          foreignField: "subject",
          as: "tests",
        },
      },
      {
        $project: {
          name: 1,
          total: { $size: "$tests" },
          completed: {
            $size: {
              $filter: {
                input: "$tests",
                as: "test",
                cond: { $eq: ["$$test.isCompleted", true] },
              },
            },
          },
        },
      },
      {
        $project: {
          name: 1,
          total: 1,
          completed: 1,
          incomplete: { $subtract: ["$total", "$completed"] },
        },
      },
    ]);

    // console.log(categoriesWithTests);

    // const moderators = await mediatorModel.find().countDocuments();
    // const verifiedUsers = await userModel
    //   .find({ isVerified: true })
    //   .countDocuments();
    // const unverifiedUsers = await userModel
    //   .find({ isVerified: false })
    //   .countDocuments();

    const subjectWithCoverdTopics = await subjectModel.aggregate([
      {
        $lookup: {
          from: "topics",
          localField: "topics",
          foreignField: "_id",
          as: "Alltopic",
        },
      },
      { $unwind: "$Alltopic" },
      {
        $lookup: {
          from: "tests",
          localField: "Alltopic._id",
          foreignField: "topic",
          as: "testDetails",
        },
      },
      {
        $addFields: {
          userCount: { $size: "$testDetails" },
        },
      },
      {
        $group: {
          _id: {
            subjectId: "$_id",
            subjectName: "$name",
          },
          topics: {
            $push:
              // topicId: "$Alltopic._id",
              "$Alltopic.name",
            // testCount: "$testCount",
          },
          userCounts: {
            $push: "$userCount",
          },
        },
      },

      {
        $project: {
          _id: "$_id.subjectId",
          subjectName: "$_id.subjectName",
          topics: 1,
          userCounts: 1,
          _id: 0,
        },
      },
    ]);

    const totalQuestionAndTopicWithSubject = await subjectModel.aggregate([
      {
        $lookup: {
          from: "topics",
          localField: "topics",
          foreignField: "_id",
          as: "Alltopic",
        },
      },
      {
        $lookup: {
          from: "questions",
          localField: "name",
          foreignField: "subject",
          as: "AllQuestion",
        },
      },

      {
        $addFields: {
          totalTopics: { $size: "$Alltopic" },
          totalQuestion: { $size: "$AllQuestion" },
        },
      },
      {
        $project: {
          subjectName: "$name",
          totalTopics: 1,
          totalQuestion: 1,
        },
      },
    ]);

    return res.status(200).json({
      users,
      // tests,
      // completedTests,
      // incompleteTests,
      questions,
      subject,
      // moderators,
      subjectWithTopics,
      subjectWithQuestions,
      subjectWithTests,
      totalQuestionAndTopicWithSubject,
      subjectWithCoverdTopics,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.getUsers = async (req, res) => {
  try {
    var page = req.query.page;
    var sortByDate = req.query.sortByDate;
    // const filterByStatus = req.query.filterByStatus;
    const query = {};
    // if (filterByStatus) {
    //   query.isVerified = filterByStatus;
    // }
    if (!page) page = 1;

    const totalUsers = await userModel.find(query).countDocuments();

    const totalPages = Math.ceil(totalUsers / 20);

    const users = await userModel
      .find(query)
      .sort([["createdAt", sortByDate === "asc" ? 1 : -1]])
      .skip((page - 1) * 20)
      .limit(20);

    return res.status(200).json({ users, totalPages, currentPage: page });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.getQuestions = async (req, res) => {
  try {
    const status = req.query.status;
    const filterBy = req.query.filterBy;
    var page = req.query.page;
    console.log(req.query);
    const query = {};

    if (!page) page = 1;

    if (filterBy) {
      query.subject = filterBy;
    }
    // if (filterBy) {
    //   query.topics = filterBy;
    // }

    if (!page) page = 1;

    const totalQuestions = await questionModel.find(query).countDocuments();
    const totalPages = Math.ceil(totalQuestions / 20);

    // if (status) {
    //   if (status !== "active" && status !== "inactive")
    //     return res.status(400).json({ error: "Invalid status" });
    // //   const questions = await questionModel
    // //     .find({ status })
    // //     .skip((page - 1) * 20)
    // //     .limit(20);
    // //   return res
    // //     .status(200)
    // //     .json({ result: questions, totalPages, currentPage: page });
    // // }

    console.log(query);
    const questions = await questionModel
      .find(query)
      .skip((page - 1) * 20)
      .limit(20);

    return res
      .status(200)
      .json({ result: questions, totalPages, currentPage: page });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.getQuestion = async (req, res) => {
  const { id } = req.params;
  try {
    const question = await questionModel.findById(id);

    return res.status(200).json({ result: question });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.addQuestion = async (req, res) => {
  try {
    const {
      img,
      question,
      answer,
      option1,
      option2,
      option3,
      option4,
      subject,
      topics,
    } = req.body;

    const { error } = addQuestionValidation.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message });

    if (
      answer !== option1 &&
      answer !== option2 &&
      answer !== option3 &&
      answer !== option4
    )
      return res
        .status(400)
        .json({ error: "Answer must be one of the options" });

    const newQuestion = await questionModel.create({
      img: img ? img : null,
      question,
      option1,
      option2,
      option3,
      option4,
      answer,
      subject,
      topics,
    });

    return res
      .status(201)
      .json({ result: newQuestion, message: "Question added successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something Went wrong" });
  }
};

routes.addBulkQuestions = async (req, res) => {
  try {
    const uploadValidFlag =
      req?.query?.uploadValidFlag === "true" ? true : false;
    // const ignoreValidation = req?.query?.ignoreValidation === "true" ? true : false;
    const { subjectId, questions } = req.body;
    const subject = await subjectModel.findById(subjectId);
    if (!subject) return res.status(404).json({ error: "subject not found" });

    // questions.forEach((question, index) => {
    //   const { error } = addQuestionValidation.validate({
    //     ...question,
    //     category: category.name,
    //   });

    // });

    const invalid = questions.reduce((acc, question, index) => {
      const { error } = addQuestionValidation.validate({
        ...question,
        subject: subject.name,
        topics: topics.name,
      });
      if (error) {
        acc.push(index);
      }
      return acc;
    }, []);

    // indexes of questions with invalid answer
    const invalidAnswers = questions.reduce((acc, question, index) => {
      if (
        question.answer !== question.option1 &&
        question.answer !== question.option2 &&
        question.answer !== question.option3 &&
        question.answer !== question.option4
      ) {
        acc.push(index);
      }
      return acc;
    }, []);

    if (!uploadValidFlag) {
      var invalidAms = invalidAnswers.map((index) => index + 1);
      var invalidQues = invalid.map((index) => index + 1);

      var errorMessage = "";

      if (invalidAms.length > 0) {
        errorMessage += `Answer must be one of the options in question ${invalidAms.join(
          ", "
        )}. `;
      }

      if (invalidQues.length > 0) {
        errorMessage += `Invalid format in question ${invalidQues.join(
          ", "
        )}. `;
      }

      if (invalidAms.length > 0 || invalidQues.length > 0) {
        return res.status(400).json({ error: errorMessage });
      }
    }

    //filter out invalidAnswers and invalid

    const validQuestions = questions.filter(
      (question, index) =>
        !invalid.includes(index) && !invalidAnswers.includes(index)
    );

    const newQuestions = await questionModel.insertMany(
      validQuestions.map((question) => ({
        ...question,
        subject: subject.name,
        topics: topics.name,
      }))
    );

    return res.status(201).json({ message: "Questions added successfully" });
  } catch (error) {
    console.log(error);
    res.status(422).json({ error: error.message });
  }
};

routes.updateQuestion = async (req, res) => {
  try {
    const { id } = req.params;
    const {
      img,
      question,
      answer,
      option1,
      option2,
      option3,
      option4,
      subject,
      topics,
    } = req.body;

    const { error } = addQuestionValidation.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message });

    const updatedQuestion = await questionModel.findByIdAndUpdate(
      id,
      {
        img: img ? img : null,
        question,
        option1,
        option2,
        option3,
        option4,
        answer,
        subject,
        topics,
      },
      { new: true }
    );

    return res.status(200).json({
      result: updatedQuestion,
      message: "Question updated successfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.deleteQuestion = async (req, res) => {
  try {
    const { id } = req.params;

    await questionModel.findByIdAndDelete(id);

    return res.status(200).json({ message: "Question deleted successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

// routes.makeQuestionPaper = async (req, res) => {
//     try {
//         const { questionIds, time, difficulty,  } = req.body;

//         if(!questionIds) return res.status(400).json({ error: "Please enter all the fields" });

//         const questionPaper = await questionPaperModel.create({
//             questions:questionIds,
//             time,
//             difficulty,
//         });

//         return res.status(200).json({ result: questionPaper, message: "Question paper generated successfully" });

//     } catch (error) {
//         console.log(error);
//         res.status(500).json({ error: "Something went wrong" });
//     }
// }

// routes.getQuestionPapers = async (req, res) => {
//     try {
//         // const { id } = req.params;

//         const questionPaper = await questionPaperModel.find().populate("questions");

//         return res.status(200).json({ result: questionPaper });

//     } catch (error) {
//         console.log(error);
//         res.status(500).json({ error: "Something went wrong" });
//     }
// }

// routes.getQuestionPaper = async (req, res) => {
//     try {
//         const { id } = req.params;

//         const questionPaper = await questionPaperModel.findById(id).populate("questions");

//         return res.status(200).json({ result: questionPaper });

//     } catch (error) {
//         console.log(error);
//         res.status(500).json({ error: "Something went wrong" });
//     }
// }

routes.addSubject = async (req, res) => {
  try {
    const { subjectName, topic } = req.body;

    if (!subjectName || !topic)
      return res.status(400).json({ error: "Please enter all the fields" });

    const ifSubjectExists = await subjectModel
      .findOne({ name: subjectName })
      .populate("topics");
    console.log(ifSubjectExists);
    // ifSubjectExists.topics.forEach((t)=>{
    //                 if(t.name === topic) return res.status(400).json({ error: "Topic already exists" });
    // })

    //  if(!ifSubjectExists){
    //    return res.status(400).json({ error: "Subject already exists" });
    //  }

    let urls = [];
    if (req.files) {
      urls = await Promise.all(
        req.files?.map(async (file) => {
          const data = await uploadFile(
            file,
            `subjectTutorial/${subjectName}-${file.originalname}`
          );
          return data.Key;
        })
      );
    }

    const newTopic = await topicModel.create({
      name: topic,
      subject: subjectName,
    });

    if (ifSubjectExists !== null) {
      const existingTopic = ifSubjectExists.topics.find(
        (t) => t.name === topic
      );
      if (existingTopic) {
        return res
          .status(400)
          .json({ error: "Topic already exists for this subject" });
      }

      ifSubjectExists.topics.push(newTopic);
      ifSubjectExists.urls = [...ifSubjectExists.urls, ...urls];
      await ifSubjectExists.save();

      return res
        .status(200)
        .json({ result: ifSubjectExists, message: "Topic added successfully" });
    }

    const newSubject = await subjectModel.create({
      name: subjectName,
      topics: [newTopic._id],
      urls,
    });

    // newSubject?.topics?.push(newTopic._id);
    // await newSubject.save();

    return res
      .status(200)
      .json({ result: newSubject, message: "Subject added successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something Went wrong" });
  }
};

routes.uploadFileInSubject = async (req, res) => {
  try {
    const { id } = req.params;

    const subject = await subjectModel.findById(id);

    if (!subject) return res.status(404).json({ error: "subject not found" });
    const urls = await Promise.all(
      req.files.map(async (file) => {
        const data = await uploadFile(
          file,
          `subjectTutorial/${subject.name}-${file.originalname}`
        );
        return data.Key;
      })
    );

    subject.urls = [...subject.urls, ...urls];
    // remove repeted urls
    subject.urls = subject.urls.filter(
      (url, index) => subject.urls.indexOf(url) === index
    );

    await subject.save();

    return res.status(200).json({ message: "File uploaded successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something Went wrong" });
  }
};

routes.deleteSubject = async (req, res) => {
  try {
    const { _id } = req.body;

    const subject = await subjectModel.findById(_id);

    if (!subject) return res.status(404).json({ error: "Subject not found" });

    const allSubjectTopics = await topicModel.find({ subject: subject.name });
    const allTopicsId = allSubjectTopics.map((topics) => topics._id);

    const allSubjectTest = await testModel.find({ subject: subject._id });
    const allSubjectTestId = allSubjectTest.map((test) => test._id);
    const deletedTopics = await topicModel.deleteMany({
      _id: { $in: allTopicsId },
    });
    const deletedTest = await testModel.deleteMany({
      _id: { $in: allSubjectTestId },
    });

    const updatedUser = await userModel.updateMany(
      {},
      { $pull: { test: { $in: allSubjectTestId } } }
    );
    console.log(updatedUser);

    const deletedQuestion = await questionModel.deleteMany({
      subject: subject.name,
    });

    console.log(deletedTest);

    console.log(deletedTopics);

    // console.log(subject);
    //     if (category.url) {
    //       await deleteFile(category.url);
    //     }

    await subject.deleteOne();

    return res.status(200).json({ message: "subject deleted successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.getSubjects = async (req, res) => {
  try {
    const subjects = await subjectModel.find().populate("topics");

    // const categoryNames = categories.map((category) => category.name);

    return res
      .status(200)
      .json({ result: subjects, message: "subjects fetched successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.getSubject = async (req, res) => {
  try {
    const subject = await subjectModel
      .findById(req.params.id)
      .populate("topics");
    if (!subject) return res.status(404).json({ error: "subject not found" });
    return res
      .status(200)
      .json({ result: subject, message: "subject fetched successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.deleteSubjectUrl = async (req, res) => {
  try {
    const { id } = req.params;
    const { url } = req.body;

    const subject = await subjectModel.findById(id);

    if (!subject) return res.status(404).json({ error: "subject not found" });

    subject.urls = subject.urls.filter((u) => u !== url);

    const data = await deleteFile(url);
    console.log(data);
    await subject.save();

    return res.status(200).json({ message: "File deleted successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something Went wrong" });
  }
};

routes.addTopics = async (req, res) => {
  try {
    const { name, subject } = req.body;
    if (!name)
      return res.status(400).json({ error: "Please enter all the fields" });

    if (!subject)
      return res.status(400).json({ error: "Please enter all the fields" });

    const ifSubjectExists = await subjectModel.findOne({ name: subject });

    if (!ifSubjectExists)
      return res.status(400).json({ error: "Subject does not exists" });

    const newTopic = await topicModel.create({
      name,
      subject: subject,
    });

    ifSubjectExists.topics.push(newTopic._id);
    await ifSubjectExists.save();
    return res
      .status(200)
      .json({ result: newTopic, message: "Topic added successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something Went wrong" });
  }
};

routes.deleteTopic = async (req, res) => {
  const { topicId, subjectId } = req.body;
  const { userId } = req;
  console.log(topicId);
  let topic;
  try {
    // let subject = await subjectModel.findById(subjectId);

    // remove all test releted to topic
    topic = await topicModel.findById(topicId);
    const allTestByTopic = await testModel.find({ topic: topic._id });
    const allTestIdByTopic = allTestByTopic.map((test) => test._id);
    const deletedTest = await testModel.deleteMany({
      _id: { $in: allTestIdByTopic },
    });

    // remove all question releted to topic
    const allQuestions = await questionModel.find({ topics: topic.name });
    const allQuestionIdByTopic = allQuestions.map((q) => q._id);
    const deletedQuestion = await questionModel.deleteMany({
      _id: { $in: allQuestionIdByTopic },
    });

    const allUsers = await userModel.updateMany(
      {},
      { $pull: { test: allTestIdByTopic } }
    );

    console.log("allUsers=", allUsers);
    console.log("deletedTest=", deletedTest);

    if (!topic) return res.status(404).json({ error: "topic not found" });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "Something went wrong" });
  }

  // if (!topic) return res.status(404).json({ error: "topic not found" });
  // await topic.deleteOne();

  // subject = await subjectModel.findById(subjectId);
  // subject.topics.pull(topicId);
  // await subject.save();
  // console.log(subject);

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await topic.deleteOne({ session: sess });
    let subject = await subjectModel.findById(subjectId);
    let user = await testModel.find({ topic: topicId });

    console.log("user=", user);
    subject.topics.pull(topicId);
    await subject.save({ session: sess });
    await sess.commitTransaction();
    return res
      .status(200)
      .json({ result: subject, message: "topic deleted successfully" });
  } catch (err) {
    return res.status(500).json({ message: "Something went wrong" });
  }
};

routes.getTopics = async (req, res) => {
  try {
    const { subject } = req.body;
    console.log(subject);
    const topics = await topicModel.find({ subject: subject });
    console.log(topics);
    if (!topics.length)
      return res.status(404).json({ message: "Topics Not Found" });
    return res
      .status(200)
      .json({ result: topics, message: "topics fetched successfully" });
  } catch (err) {
    return res.status(500).json({ error: "Somthing went wrong" });
  }
};

routes.getTopic = async (req, res) => {
  try {
    const topic = await topicModel.findById(req.params.id);
    if (!topic) return res.status(404).json({ error: "topic not found" });
    return res
      .status(200)
      .json({ result: topic, message: "topic fetched successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.getTests = async (req, res) => {
  try {
    var page = req.query.page;
    const sortBy = req.query.sortBy;

    const orderBy = req.query.orderBy === "asc" ? 1 : -1;

    // const filterByCompletion = req.query.filterByCompletion;
    const filterByTopics = req.query.filterByTopics;
    const query = {};

    // if (filterByCompletion) {
    //   query.isCompleted = filterByCompletion === "true" ? true : false;
    // }

    if (filterByTopics) {
      query.topics = filterByTopics;
    }

    if (!page) page = 1;
    const totalTests = await testModel.find(query).countDocuments();

    const totalPages = Math.ceil(totalTests / 20);

    const tests = await testModel
      .find(query)
      .sort([[sortBy, orderBy]])
      .skip((page - 1) * 20)
      .limit(20)
      .populate("subject", "name")
      .populate("topic", "name")
      .populate("user", "name");

    return res.status(200).json({
      result: tests,
      totalPages,
      message: "Tests fetched successfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.getTest = async (req, res) => {
  try {
    const test = await testModel
      .findById(req.params._id)
      .populate("subject user topic");

    if (!test) return res.status(200).json({ error: "Test not found" });

    const questions = test.questions.map((question, index) => {
      return {
        question: question.question,
        Option1: question.option1,
        Option2: question.option2,
        Option3: question.option3,
        Option4: question.option4,
        answer: question.answer,
        myAnswer: test.answers[index],
      };
    });

    return res.status(200).json({
      user: test?.user.name,
      subject: test?.subject.name,
      topic: test?.topic.name,
      image: test?.user.profilePicture,
      score: test.score,
      completion: test.isCompleted,
      result: questions,
      message: "Test fetched Successfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.addMediator = async (req, res) => {
  try {
    const { name, userId, password } = req.body;

    const { error } = addMediatorValidation.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message });

    const ifExists = await mediatorModel.findOne({ userId });

    if (ifExists)
      return res.status(400).json({ error: "UserId already exists" });

    const bcryptPassword = await bcrypt.hash(password, 12);
    const mediator = await mediatorModel.create({
      name,
      userId,
      password: bcryptPassword,
    });

    return res
      .status(201)
      .json({ result: mediator, message: "registered successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something Went wrong" });
  }
};

routes.getMediators = async (req, res) => {
  try {
    const mediators = await mediatorModel.find();

    return res.status(200).json({ result: mediators });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something Went wrong" });
  }
};

routes.deleteMediator = async (req, res) => {
  try {
    const { id } = req.params;

    await mediatorModel.findByIdAndDelete(id);

    return res.status(200).json({ message: "Mediator deleted successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something Went wrong" });
  }
};

routes.uploadTestInstruction = async (req, res) => {
  try {
    console.log(req);
    if (req?.files[0]?.mimetype !== "video/mp4")
      return res.status(400).json({ error: "Please upload a video file" });
    const fileName = "tutorial.mp4";
    const data = await uploadFile(req?.files[0], `appTutorial/${fileName}`);
    return res
      .status(200)
      .json({ data, message: "File uploaded successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Something Went wrong" });
  }
};

routes.generateQrCode = async (req, res) => {
  try {
    const timestamp = new Date().toISOString();
    const { date, topics } = req.body;

    var ciphertext = CryptoJS.AES.encrypt(
      timestamp,
      "secret key 123"
    ).toString();

    //   var bytes = CryptoJS.AES.decrypt(ciphertext, "secret key 123");
    //   var originalText = bytes.toString(CryptoJS.enc.Utf8);

    console.log(ciphertext); // 'my message'

    const id = req.params.id;

    const subject = await subjectModel.findById(id);
    const topic = await topicModel.findById(topics);

    const questions = await questionModel.find({
      subject: subject.name,
      topics: topic.name,
    });

    if (questions.length < 5) {
      return res.status(400).json({
        error:
          "There should be minimum 5 questions in this topic to qenerate QR code",
      });
    }

    if (!subject) {
      return res.status(400).json({
        message: "Invalid subject",
      });
    }
    if (!topic) {
      return res.status(400).json({
        message: "Invalid topic",
      });
    }

    if (!timestamp) {
      return res.status(400).json({
        message: "Text is required",
      });
    }

    const Code = await qrcodeModel.create({
      qrcode: timestamp,
      subject: id,
      topics: topics,
      expiresAt: date,
    });

    if (!Code) {
      return res.status(500).json({
        message: "try again later",
      });
    }

    const qrCode = qr.image(ciphertext, { type: "png" });
    qrCode.pipe(res);
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      message: "try again later",
      error: error.message,
    });
  }
};

module.exports = routes;

routes.getTestStatus = async (req, res) => {
  try {
    const { id } = req.params;
    console.log(id);

    const test = await testModel
      .find({ user: id })
      .populate("user")
      .populate("subject");

    const user = await userModel.findById(new mongoose.Types.ObjectId(id));
    console.log(user);

    // if (!test.length<=0) {
    //   console.log("if");
    //   return res.status(404).json({
    //     message: "test not found",
    //   });
    // }

    if (!test.length) {
      return res.status(200).json({
        result: {
          _id: user._id,
          name: user.name,
          totalScore: 0,
          average: 0,
          testSubjects: "",
          coveredTopics: 0,
          leftTopics: 0,
        },
        message: "test Not found",
      });
    }

    const totalScore = test.reduce((acc, curr) => acc + curr.score, 0);
    const average = totalScore / test.length;

    const testSubjects = [...new Set(test.map((t) => t.subject))];
    console.log(testSubjects);
    const totalTopics = testSubjects.reduce(
      (acc, curr) => acc + curr.topics?.length,
      0
    );
    const coveredTopics = test.map((t) => t.topic);
    const leftTopics = totalTopics - coveredTopics.length;

    console.log(totalScore, average, testSubjects, totalTopics, coveredTopics);

    return res.status(200).json({
      result: {
        _id: user._id,
        name: user.name,
        totalScore: totalScore,
        average: average,
        testSubjects: testSubjects.map((s) => s.name),
        coveredTopics: coveredTopics.length,
        leftTopics: leftTopics,
      },
      message: "succes",
    });
  } catch (error) {
    return res.status(500).json({
      message: "Some tthing went wrong",
      error: error.message,
    });
  }
};

routes.leaderBoard = async (req, res) => {
  try {
    let { page, sortByRank, filterBySubject, filterByTopic } = req.query;
    page = parseInt(page, 10) || 1;
    const limit = 20;
    const skip = (page - 1) * limit;


    const query = {};
    const testFilter = {};

    if (filterBySubject) {
      query["testInfo.subject"] = new mongoose.Types.ObjectId(filterBySubject);
      testFilter.$and = [
        { subject: new mongoose.Types.ObjectId(filterBySubject) },
      ];
    }
    if (filterByTopic) {
      query["testInfo.topic"] = new mongoose.Types.ObjectId(filterByTopic);
      testFilter.$and.push({
        topic: new mongoose.Types.ObjectId(filterByTopic),
      });
    }

    console.log("query=", query);
    console.log("testFilter=", testFilter);

    // Aggregation pipeline
    const aggregationPipeline = [
      {
        $lookup: {
          from: "tests",
          localField: "test",
          foreignField: "_id",
          as: "testInfo",
        },
      },

      {
        $unwind: "$testInfo",
      },
      {
        $match: query,
      },
      {
        $group: {
          _id: "$_id",
          userName: { $first: "$name" },
          totalTests: { $sum: 1 },
          totalScore: { $sum: "$testInfo.score" },
          averageScore: { $avg: "$testInfo.score" },
          correctCount:{ $sum: "$correctCount" },
          inCorrectCount:{ $sum: "$inCorrectCount" }
        },
      },
    ];

    if (sortByRank) aggregationPipeline.push({ $sort: { averageScore: -1 } });

    aggregationPipeline.push({ $skip: skip }, { $limit: limit });

    // Getting the aggregated user data
    const aggregatedUsers = await userModel.aggregate(aggregationPipeline);
    aggregatedUsers;

    // Count total users matching the query
    const totalUsers = await userModel.aggregate([
      ...aggregationPipeline.slice(
        0,
        aggregationPipeline.findIndex((stage) => stage.$skip)
      ),
      { $count: "total" },
    ]);

    const totalPages = Math.ceil((totalUsers[0]?.total || 0) / limit);

    let  users = await userModel
      .find({$expr: { $gt: [{ $size: "$test" }, 0] }})
      .populate({
        path: "test",
        match: testFilter,
      });
users=users.filter((user)=>user.test.length !== 0)


    // // Calculate correct and incorrect answers

    users.forEach((user, index) => {
      let correctCount = 0;
      let inCorrectCount = 0;

      user.test.forEach((t) => {
        t.questions.forEach((q, index) => {
         
          if (q.answer === t.answers[index]) {
            correctCount++;
          } else {
            inCorrectCount++;
          }
        });
      });
      aggregatedUsers.forEach((aggregatedUser)=>{
            if(aggregatedUser._id.toString() === user._id.toString()){
              aggregatedUser.correctCount = correctCount;
              aggregatedUser.inCorrectCount = inCorrectCount;
            }
      })
    });

  
    
    

    return res.status(200).json({ users: aggregatedUsers, totalPages });

    //  return res.status(200).json({
    //   users: users.map((user) => {
    //     return {
    //       _id: user.user._id,
    //       name: user.user.name,
    //     };
    //   }),
    //   totalPages,
    //   totalScore: users.map((user) => user.totalScore),
    //   averageScore: users.map((user) => user.averageScore),
    //   correctAns,
    //   inCorrectAns,
    // });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Something went wrong" });
  }
};

routes.averageReports = async (req, res) => {
  try {
    // const users=await userModel.find({},{password:0}).populate({path:"test",populate:{path:"subject",populate:{path:"topics"}}})

    const allUsers = await userModel
      .find()
      .populate({ path: "test", populate: { path: "subject topic" } });
    const allSubject = await subjectModel.find();

    console.log(allUsers);

   

    // if(!allUsers.test){
    //   return res
    //   .status(200)
    //   .json({ result: test, message: "succes" });
    // }
    // const responseData = allUsers
    //   ?.map((user) => {
    //     if (user.test) {
    //       return allSubject?.map((subject) => {
    //         const coveredTopics = user?.test.reduce((count, t) => {
    //           if (t?.subject?.name === subject.name) {
    //             return (
    //               count +
    //               subject?.topics?.filter(
    //                 (topic) => topic.toString() === t.topic._id.toString()
    //               ).length
    //             );
    //           }
    //           return count;
    //         }, 0);

    //         const totalTopics = subject.topics.length;
    //         const leftTopics = totalTopics - coveredTopics;

    //         return {
    //           _id: user._id,
    //           name: user.name,
    //           subject: subject.name,
    //           totalTopics,
    //           coveredTopics,
    //           leftTopics,
    //         };
    //       });
    //     }
    //   })
    //   .flat();



    const responseData = allUsers
      ?.map((user) => {
        let allSubjectDetails;
        if (user.test) {
           allSubjectDetails= allSubject?.map((subject) => {
            const coveredTopics = user?.test.reduce((count, t) => {
              if (t?.subject?.name === subject.name) {
                return (
                  count +
                  subject?.topics?.filter(
                    (topic) => topic.toString() === t.topic._id.toString()
                  ).length
                );
              }
              return count;
            }, 0);

            const totalTopics = subject.topics.length;
            const leftTopics = totalTopics - coveredTopics;

            return {
              subject:subject.name,
              totalTopics,
              coveredTopics,
              leftTopics,
            };
          });
        }
        return {
          _id: user._id,
          name: user.name,
          subject:allSubjectDetails.map(subject=>subject.subject),
          totalTopics:allSubjectDetails.map(subject=>subject.totalTopics),
          coverdTopics:allSubjectDetails.map(subject=>subject.coveredTopics),
          leftTopics:allSubjectDetails.map(subject=>subject.leftTopics),
          
        };   
      })

    let page = req.query.page;
    const limit = 20;
    const totalPages = Math.ceil(responseData.length / limit);

    if (!page) page = 1;

    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    const paginateResponseData = responseData.slice(startIndex, endIndex);

    console.log("responseData=", responseData.length);

    return res
      .status(200)
      .json({ result: paginateResponseData, totalPages, message: "succes" });
  } catch (error) {
    console.log(error.message);
    return res.status(500).json({ message: "Somthing wrong" });
  }
};
