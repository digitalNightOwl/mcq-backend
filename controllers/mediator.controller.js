const qr = require("qr-image");
var CryptoJS = require("crypto-js");
const qrcodeModel = require("../models/qrcode.model");
const mediatorModel = require("../models/mediator.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const questionModel = require("../models/question.model");
const routes = {};

routes.login = async (req, res) => {
  const { userId, password } = req.body;

  if (!userId || !password) {
    return res.status(400).json({
      message: "All fields are required",
    });
  }

  const mediator = await mediatorModel.findOne({ userId: userId });

  if (!mediator) {
    return res.status(400).json({
      message: "Invalid credentials",
    });
  }

  const isMatch = await bcrypt.compare(password, mediator.password);

  if (!isMatch) {
    return res.status(400).json({
      message: "Invalid credentials",
    });
  }

  const token = jwt.sign(
    {
      id: mediator._id,
    },
    process.env.JWT_SECRET
  );

  res.status(200).json({
    token: token,
    userId: mediator.userId,
    name: mediator.name,
  });
};

routes.generateQrCode = async (req, res) => {
  try {
    const timestamp = new Date().toISOString();
    const date = req.body.date;

    var ciphertext = CryptoJS.AES.encrypt(
      timestamp,
      "secret key 123"
    ).toString();

    //   var bytes = CryptoJS.AES.decrypt(ciphertext, "secret key 123");
    //   var originalText = bytes.toString(CryptoJS.enc.Utf8);

    console.log(ciphertext); // 'my message'

    const id = req.params.id;

    const category = await categoryModel.findById(id);

    const questions = await questionModel.find({category:category.name})

    if(questions.length < 5){
      return res.status(400).json({
        message: "There should be minimum 5 questions in this category to qenerate QR code",
      });
    }

    if (!category) {
      return res.status(400).json({
        message: "Invalid category",
      });
    }

    if (!timestamp) {
      return res.status(400).json({
        message: "Text is required",
      });
    }

    const Code = await qrcodeModel.create({
      qrcode: timestamp,
      category: id,
      expiresAt: date,
    });

    if (!Code) {
      return res.status(500).json({
        message: "try again later",
      });
    }

    const qrCode = qr.image(ciphertext, { type: "svg" });
    //   res.type("");
    qrCode.pipe(res);
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      message: "try again later",
      error: error.message,
    });
  }
};

module.exports = routes;
