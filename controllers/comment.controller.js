const Admin = require("../models/admin.model");
const commentModel = require("../models/comment.model");
const Comment = require("../models/comment.model");
const testModel = require("../models/test.model");
const User = require("../models/user.model");
const sendNotification = require("../utils/sendNotification");

const routes = {};

routes.createComment = async (req, res) => {
  let { senderType, content, senderId } = req.body;
  console.log(req.body);

  if (senderType === "admin") {
    senderId = req.userId;
  }

  const timestamp = new Date().toISOString();
  const receiverId = req.params.id;

  console.log({ senderType, content });

  try {
    const [receiver, sender] = await Promise.all([
      senderType === "admin"
        ? testModel.findById(receiverId)
        : Admin.findById(receiverId),
      senderType === "admin"
        ? Admin.findById(senderId)
        : testModel.findById(senderId),
    ]);

    console.log(receiver?.message);

    console.log("receiver=", receiver, "sender=", sender);

    if (!receiver || !sender) {
      return res.status(404).json({ error: "Test not found" });
    }

    let fcmToken;

    const newComment = await Comment.create({
      senderId: sender._id,
      receiverId: receiver._id,
      senderType,
      content,
      timestamp,
    });

  

    sendNotification({
      type: "comment",
      typeId: newComment?._id,
      body: newComment.content,
      // data: data,
      deviceToken: fcmToken,
    });

    res.json({ message: "Comment created successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

routes.getCommentByUser = async (req, res) => {
  try {
    const messages = await commentModel.find(
      { $or: [{ senderId: req.params.id }, { receiverId: req.params.id }] },
      {
        content: 1,
        isRead: 1,
        timestamp: 1,
        senderType: 1,
        senderId:1
      }
    );
    const userImage= await testModel.findById(req.params.id).populate("user","profilePicture")
    if (!messages) return res.status(404).json({ message: "Test not found" });
    res.status(200).json({result:{messages,image:userImage?.user?.profilePicture}, message: "Message fetched Successfully " });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

routes.updateCommentByUser = async (req, res) => {
  try {
    const { senderType } = req.body;

    const allComments = await commentModel.find({
      $or: [{ senderId: req.params.id }, { receiverId: req.params.id }],
    });

    if (senderType === "admin") {
      allComments.map((comment) => {
        if (comment.senderType === "admin") {
          comment.isRead = true;
        }
      });
    } else {
      allComments.map((comment) => {
        if (comment.senderType === "user") {
          comment.isRead = true;
        }
      });

    }

    await Promise.all(allComments.map((comment) => comment.save()));

    res.status(200).json({ message: "Update Succesfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

module.exports = routes;