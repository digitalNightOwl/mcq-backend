const mongoose = require("mongoose");

const subjectSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    trim:true
  },
  
  topics: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "Topic",
    trim:true,
    default: [],
  },

  urls:{
    type: Array,
    default: [],
},

});

module.exports = mongoose.model("Subject", subjectSchema);
