const mongoose = require('mongoose');

const qrcodeSchema = new mongoose.Schema({
    qrcode:{
        type:String,
        required:true,
    },
    subject:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"Subject",
    },

    topics:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"Topic",
    },
    expiresAt:{
        type:Date,
        required:true,
    },
})

module.exports = mongoose.model("Qrcode",qrcodeSchema);