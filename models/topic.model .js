const mongoose = require('mongoose');

const topicSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        trim:true
    },
    subject:{
       type:String,
       required:true,
       trim:true 
    },
    appear:{
    type:Boolean,
    default:false
    }
   
})

module.exports = mongoose.model('Topic',topicSchema);
