const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: [true, "Email already exists"],
  },
  password: {
    type: String,
    required: true,
    length: [6, "Password must be at least 6 characters"],
  },
  employeeId: {
    type: String,
    required: true,
    unique: [true, "Employee Id already exists"],
  },
  test: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "Test",
    default: [],
  },
  otpExpires:{
    type:Date,
    default:Date.now,
},
  profilePicture:{
      type:String,
      default:null
  },
  fcmToken:{
    type:String,
    default:null
  },
 
  createdAt: {
    type: Date,
    default: () => new Date(),
  },
});

module.exports = mongoose.model("User", userSchema);
