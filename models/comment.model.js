const mongoose = require("mongoose");

const commentSchema = new mongoose.Schema({
  senderId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  receiverId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Test",
    required: true,
  },
  isRead: {
    type: Boolean,
    default: false,
  },
  senderType: {
    type: String,
    enum: ["admin", "user"],
    default: "user",
  },
  content: {
    type: String,
    required: true,
  },
 
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Comment", commentSchema);
