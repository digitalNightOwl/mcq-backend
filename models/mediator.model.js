const mongoose = require('mongoose');

const mediatorSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    userId:{
        type:String,
        required:true,
    },
    password:{
        type:String,
        required:true,
    },
});

module.exports = mongoose.model('Mediator',mediatorSchema);