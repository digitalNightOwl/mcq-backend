const mongoose = require("mongoose");

const adminSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  fcmToken:{
    type:String,
    default:null
  }
  // categories: {
  //   type: Array,
  //   default: [],
  // },
});

module.exports = mongoose.model("Admin", adminSchema);
