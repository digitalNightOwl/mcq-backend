require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const userRouter = require('./routes/user.routes');
const adminRouter = require('./routes/admin.routes');
const mediatorRouter = require('./routes/mediator.routes');

const app = express();


app.use(cors());
app.use(morgan('dev')); 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use("/mediator",mediatorRouter);
app.use("/user",userRouter);

app.use("/admin",adminRouter);

app.use("/",(req,res)=>{
    res.send("i am alive buddy 😊");
})

app.use((req,res)=>{
    res.status(404).send("page not found");
})
 
app.listen(4000,()=>{
    console.log(`server is running on port 80`);
})

mongoose.connect(process.env.MONGODB_URI,{
    // useNewUrlParser: true,
    // useUnifiedTopology: true,
}).then(()=>{
    console.log(`database connected successfully`);
})

// data added